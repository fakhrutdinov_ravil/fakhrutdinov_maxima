package ru.ravil.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.ravil.dto.MovieDto;
import ru.ravil.models.Actor;
import ru.ravil.models.Movie;
import ru.ravil.service.ActorService;
import ru.ravil.service.MovieService;

import java.util.List;

@Controller
public class MoviesController {

  private final MovieService movieService;
  private final ActorService actorService;

  @Autowired
  public MoviesController(MovieService movieService, ActorService actorService) {
    this.movieService = movieService;
    this.actorService = actorService;
  }

  //При заходе на url /movies отдает нам страничку movies
  @RequestMapping(value = "/movies")
  public String start() {
    return "movies";
  }

  //Метод для работы с POST запросами и JSON объектами.
  //При загрузке страницы в JS вызывается функция: function getAllMovies()
  //Отправляет на этот метод объект Movie с пустым полем movieName.
  //Где мы уже делаем проверку, что делать с таким объектом, в коде ниже описано.
  @RequestMapping(value = "/movies", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ResponseBody
  @ResponseStatus(HttpStatus.CREATED)
  public List<MovieDto> addMovieAndGetAll(@RequestBody Movie movie) {
    if (movie.getMovieName() != null) {
      movieService.addMovie(movie);
    }
    return movieService.getAllMovies();
  }

  @RequestMapping("/movies/{movie-id}/actors")
  public String getActorsFromMovies(@PathVariable("movie-id") Long movieId, Model model) {
    model.addAttribute("actors", actorService.getAllByMovieId(movieId));
    model.addAttribute("actorsWithoutMovie", actorService.getAllByMoviesIsNull());
    return "actors_from_movie";
  }

  @RequestMapping(value = "/movies/{movie-id}/actors", method = RequestMethod.POST)
  public String addActorFromMovie(@PathVariable("movie-id") Long movieId, Actor actor) {
    movieService.addActorFromMovie(movieId, actor);
    return "redirect:/movies/" + movieId + "/actors";
  }
}
