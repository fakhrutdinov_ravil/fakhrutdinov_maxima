package ru.ravil.dto;

import lombok.Builder;
import lombok.Data;
import ru.ravil.models.Actor;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@Data
public class ActorDto {
  private Long id;
  private String firstName;
  private String lastName;
  private String age;

  public static ActorDto from(Actor actor) {
    return ActorDto.builder()
        .id(actor.getId())
        .age(actor.getAge())
        .firstName(actor.getFirstName())
        .lastName(actor.getLastName())
        .build();
  }

  public static List<ActorDto> from(List<Actor> actorList){
    return actorList.stream().map(ActorDto::from).collect(Collectors.toList());
  }
}
