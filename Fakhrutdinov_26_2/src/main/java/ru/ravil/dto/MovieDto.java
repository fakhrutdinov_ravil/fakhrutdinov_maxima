package ru.ravil.dto;

import lombok.Builder;
import lombok.Data;
import ru.ravil.models.Movie;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class MovieDto {
  private long id;
  private String movieName;

  public static MovieDto from(Movie movie) {
    return MovieDto.builder()
        .id(movie.getId())
        .movieName(movie.getMovieName())
        .build();
  }

  public static List<MovieDto> from(List<Movie> movieList) {
    return movieList.stream().map(MovieDto::from).collect(Collectors.toList());
  }
}
