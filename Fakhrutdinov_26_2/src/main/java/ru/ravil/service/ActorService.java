package ru.ravil.service;

import ru.ravil.dto.ActorDto;

import java.util.List;

public interface ActorService {
  List<ActorDto> getAllByMovieId(Long movieId);
  List<ActorDto> getAllByMoviesIsNull();
}
