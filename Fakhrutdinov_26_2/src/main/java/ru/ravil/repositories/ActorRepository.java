package ru.ravil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ravil.models.Actor;

import java.util.List;
import java.util.Optional;

public interface ActorRepository extends JpaRepository<Actor,Long> {
  List<Actor>  getAllByMovies_id(long movies_id);
  List<Actor> getAllByMoviesIsNull();
  Optional<Actor> getById(long id);
}
