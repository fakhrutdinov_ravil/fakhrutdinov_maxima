package servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class SearchPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        try (PrintWriter writer = resp.getWriter()) {
            writer.println("<h1>Google</h1>");
            writer.println("<p>Введите текст для поиска и получите нужный результат!</p>");
            writer.println("<br>");
            writer.println("<form action=http://google.com/search>");
            writer.println("<label for=search>Тут надо вводить запрос:</label>");
            writer.println("<input id=search type=text name=q placeholder=Введите поисковый запрос>");
            writer.println("<input type=submit name= value=ПОИСК>");
            writer.println("</form>");
        }
    }
}
