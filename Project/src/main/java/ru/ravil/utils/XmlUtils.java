package ru.ravil.utils;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.ravil.models.Department;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


/**
 * XML interaction class.
 *
 * @version 1.0
 * @author Ravil Fakhrutdinov
 */
public class XmlUtils {
    private static Properties properties;

    public static void setProperties(Properties properties) {
        XmlUtils.properties = properties;
    }

    /**
     * Connection source object.
     */
    private static DataSource dataSource;

    /**
     * Logger object.
     */
    private static final Logger logger = Logger.getLogger(XmlUtils.class);

    /**
     * Constructor - creating a new object.
     * @param dataSource Connection source object.
     */
    public XmlUtils(DataSource dataSource) {
        XmlUtils.dataSource = dataSource;
    }

    /**
     * The method parses an XML file into a Map containing departments.
     * @param fileName The file name that will be parsed.
     * @return Map containing departments.
     */
    public Map<Department, Department> parseXml(String fileName) {
        Map<Department, Department> departmentMap = new HashMap<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        Document document;

        File jarPath = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        String xmlPath = jarPath.getParentFile().getAbsolutePath();
        try (InputStream inputStream = new FileInputStream(xmlPath + "/" + fileName)) {
            InputSource inputSource;
            try (Reader reader = new InputStreamReader(inputStream)) {
                inputSource = new InputSource(reader);
                builder = factory.newDocumentBuilder();
                document = builder.parse(inputSource);
            }
            logger.info("Received XML file");
        } catch (ParserConfigurationException | IOException | SAXException e) {
            logger.fatal("Failed to get XML file or convert it");
            throw new IllegalStateException(e);
        }
        NodeList companyElements = document.getDocumentElement().getElementsByTagName("row");
        for (int i = 0; i < companyElements.getLength(); i++) {
            Department department;
            Node companyNode = companyElements.item(i);
            if (companyNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) companyNode;
                Node node = element.getElementsByTagName("depcode").item(0);
                String depCode = node.getTextContent();
                Node node1 = element.getElementsByTagName("depjob").item(0);
                String depJob = node1.getTextContent();
                if (isNumber(depJob)) {
                    throw new IllegalStateException("Incorrect data format, the position field cannot contain numbers");
                }
                Node node2 = element.getElementsByTagName("description").item(0);
                String description = node2.getTextContent();
                department = new Department(depCode, depJob, description);
                if (departmentMap.containsKey(department)) {
                    logger.fatal("Duplicate in XML file");
                    throw new IllegalStateException("There is already such a department with such a position.");
                }
                departmentMap.put(department, department);
            }
        }
        return departmentMap;
    }

    /**
     * This method synchronizes the file and the database.
     *
     * @param fileName The name of the file to be synchronized with the database.
     */
    public void sync(String fileName) {
        String SQL_DELETE = "delete from "+properties.getProperty("namebd")+" where depcode=? and depjob=?";
        String SQL_UPDATE = "update "+properties.getProperty("namebd")+" set description=? where depcode=? and depjob=?";
        String SQL_INSERT = "insert into "+properties.getProperty("namebd")+"(depcode, depjob, description) values (?,?,?)";

        Map<Department, Department> departmentMap = parseXml(fileName);
        logger.info("Parsing the XML file was successful");

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statementSelect = connection.prepareStatement("select * from "+properties.getProperty("namebd"));
             PreparedStatement statementDelete = connection.prepareStatement(SQL_DELETE);
             PreparedStatement statementUpdate = connection.prepareStatement(SQL_UPDATE);
             PreparedStatement statementInsert = connection.prepareStatement(SQL_INSERT)) {
            connection.setAutoCommit(false);

            try (ResultSet resultSet = statementSelect.executeQuery()) {
                logger.info("A query in the database to select all fields was successful");
                if (resultSet.next()) {
                    do {
                        Department currentDepartment;
                        String depCode = resultSet.getString("depCode");
                        String depJob = resultSet.getString("depJob");
                        String description = resultSet.getString("description");
                        currentDepartment = new Department(depCode, depJob, description);

                        if (!departmentMap.containsKey(currentDepartment)) {
                            statementDelete.setString(1, currentDepartment.getDepCode());
                            statementDelete.setString(2, currentDepartment.getDepJob());
                            statementDelete.addBatch();
                        } else {
                            Department departmentFromMap = departmentMap.get(currentDepartment);
                            statementUpdate.setString(1, departmentFromMap.getDescription());
                            statementUpdate.setString(2, departmentFromMap.getDepCode());
                            statementUpdate.setString(3, departmentFromMap.getDepJob());
                            statementUpdate.addBatch();
                            departmentMap.remove(departmentFromMap);
                        }
                    } while (resultSet.next());
                }

                for (Map.Entry<Department, Department> entry : departmentMap.entrySet()) {
                    Department department = entry.getKey();
                    try {
                        statementInsert.setString(1, department.getDepCode());
                        statementInsert.setString(2, department.getDepJob());
                        statementInsert.setString(3, department.getDescription());
                        statementInsert.addBatch();
                    } catch (SQLException e) {
                        throw new IllegalStateException(e);
                    }
                }

                int[] insertRows = statementInsert.executeBatch();
                int[] deleteRows = statementDelete.executeBatch();
                int[] updateRows = statementUpdate.executeBatch();

                logger.info("Sending a transaction");
                connection.commit();

                logger.info("The transaction was successful");
                System.out.println("Adding fields (field) was successful!");
                System.out.println("Deleting fields (field) was successful!");
                System.out.println("Updating fields (field) was successful!");
                System.out.println("Sync function completed successfully!");
                logger.info("The database request to add fields was successful, rows were added = " + insertRows.length);
                logger.info("The database request to delete fields was successful, rows were deleted = " + deleteRows.length);
                logger.info("The database request to update fields was successful, rows were updated = " + updateRows.length);
            }
        } catch (SQLException e) {
            logger.fatal("The request failed");
            throw new IllegalStateException(e);
        }
    }

    /**
     * The method unloads data about the database as an XML file.
     * @param fileName The name of the file into which the information about the database will be dumped.
     */
    public void unload(String fileName) {
        File jarPath = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        String xmlPath = jarPath.getParentFile().getAbsolutePath();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("select query_to_xml('select "
                     +properties.getProperty("namebd")
                     +".depcode, "+properties.getProperty("namebd")
                     +".depjob, description from "
                     +properties.getProperty("namebd")
                     +"',true,false,'');")) {
            if (resultSet.next()) {
                try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(xmlPath + "/" + fileName))) {
                    bufferedWriter.write(resultSet.getString("query_to_xml"));
                    bufferedWriter.newLine();
                }
                logger.info("XML file uploaded successfully");
                System.out.println("The unload function succeeded!");
            } else {
                logger.fatal("XML file was not uploaded");
                throw new IllegalStateException("Database query failed");
            }
        } catch (SQLException | IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private static boolean isNumber(String string) {
        return string.chars().allMatch(Character::isDigit);
    }
}
