package ru.ravil.utils;

import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * This class provides database connection using JDBC.
 * @author Ravil Fakhrutdinov
 * @version 1.0 Ravil Fakhrutdinov
 */
public class ManagerDataSource implements DataSource {
    /**
     * Logger object field.
     */
    private static final Logger logger = Logger.getLogger(ManagerDataSource.class);

    /**
     * Connection object field.
     */
    private Connection connection;

    /**
     * Database URL.
     */
    private String url;

    /**
     * Database username.
     */
    private String user;

    /**
     * Database user password
     */
    private String password;

    /**
     * Connection manager creation constructor.
     * @param properties Connection settings file.
     */
    public ManagerDataSource(Properties properties) {
        this.url = properties.getProperty("url");
        this.user = properties.getProperty("username");
        this.password = properties.getProperty("password");
    }

    /**
     * Method for opening a connection.
     */
    private void openConnection() {
        try {
            logger.info("Opening a connection");
            this.connection = DriverManager.getConnection(url, user, password);
            logger.info("Connection open");
        } catch (SQLException e) {
            logger.fatal("Error opening connection");
            throw new IllegalStateException(e);
        }
    }

    /**
     * Connection obtaining method.
     * @return Object connection
     * @throws SQLException Error if the connection is null or closed.
     */
    @Override
    public Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            openConnection();
        }
        return connection;
    }

    @Override
    public Connection getConnection(String username, String password) {
        return null;
    }

    @Override
    public PrintWriter getLogWriter() {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) {

    }

    @Override
    public void setLoginTimeout(int seconds) {

    }

    @Override
    public int getLoginTimeout() {
        return 0;
    }

    @Override
    public java.util.logging.Logger getParentLogger() {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) {
        return false;
    }
}
