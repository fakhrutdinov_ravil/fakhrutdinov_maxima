package ru.ravil;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import ru.ravil.utils.ManagerDataSource;
import ru.ravil.utils.XmlUtils;

import javax.sql.DataSource;
import java.io.*;
import java.util.Properties;

/**
 * The method initializes the settings file and which function will be used.
 * @author Ravil Fakhrutdinov
 * @version 1.0
 */
public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);
    public static void main(String[] args) {
        Properties properties = getProperties();
        XmlUtils.setProperties(properties);
        logger.info("Application launch");
        System.out.println("Application launch");
        DataSource dataSource = new ManagerDataSource(properties);
        XmlUtils xmlUtils = new XmlUtils(dataSource);

        if (args[0].equalsIgnoreCase("sync")) {
            logger.info("Starting the 'sync' function");
            xmlUtils.sync(args[1]);
        }
        if (args[0].equalsIgnoreCase("unload")) {
            logger.info("Start the 'unload' function");
            xmlUtils.unload(args[1]);
        }
    }

    /**
     * The method receives an object with settings.
     * @return Object with information about settings.
     */
    private static Properties getProperties() {
        Properties properties = new Properties();
        File jarPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String propertiesPath = jarPath.getParentFile().getAbsolutePath();
        try (InputStream inputStream = new FileInputStream(propertiesPath + "/properties.properties")) {
            Reader reader = new InputStreamReader(inputStream);
            properties.load(reader);
            PropertyConfigurator.configure(properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
}
