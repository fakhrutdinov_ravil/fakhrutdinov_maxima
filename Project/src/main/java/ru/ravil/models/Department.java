package ru.ravil.models;

import java.util.Objects;

/**
 * @author Ravil Fakhrutdinov
 * @version 1.0
 */
public class Department {
    /**
     * Department code field.
     */
    private String depCode;
    /**
     * Position name field.
     */
    private String depJob;
    /**
     * Comment field.
     */
    private String description;

    /**
     * Constructor - creating an object of class department.
     * @param depCode Department code.
     * @param depJob Position name.
     * @param description Comment.
     */
    public Department(String depCode, String depJob, String description) {
        this.depCode = depCode;
        this.depJob = depJob;
        this.description = description;
    }

    public String getDepCode() {
        return depCode;
    }

    public String getDepJob() {
        return depJob;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department department = (Department) o;
        return Objects.equals(depCode, department.depCode) && Objects.equals(depJob, department.depJob);
    }

    @Override
    public int hashCode() {
        return Objects.hash(depCode, depJob);
    }
}
