package ru.ravil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ravil.models.Movie;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie,Long> {
  List<Movie> getAllByMovieNameIsNotNull();
  List<Movie> getAllById(Long id);
}
