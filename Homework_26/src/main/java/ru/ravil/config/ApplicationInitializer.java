package ru.ravil.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

public class ApplicationInitializer implements WebApplicationInitializer {

  @Override
  public void onStartup(ServletContext servletContext) {
    AnnotationConfigWebApplicationContext springWebContext = new AnnotationConfigWebApplicationContext();
    springWebContext.register(ApplicationConfig.class);
    ContextLoaderListener contextLoaderListener = new ContextLoaderListener(springWebContext);
    servletContext.addListener(contextLoaderListener);

    ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("dispatcher",
        new DispatcherServlet(springWebContext));
    dispatcherServlet.setLoadOnStartup(1);
    dispatcherServlet.addMapping("/");
  }
}
