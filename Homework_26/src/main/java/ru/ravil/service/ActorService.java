package ru.ravil.service;

import ru.ravil.dto.ActorDto;
import ru.ravil.models.Actor;

import java.util.List;

public interface ActorService<T extends ActorDto> {
  List<T> getAllByMovieId(Long movieId);
  List<T> getAllByMoviesIsNull();
}
