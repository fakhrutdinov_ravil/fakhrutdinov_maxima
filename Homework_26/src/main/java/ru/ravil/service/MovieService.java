package ru.ravil.service;

import ru.ravil.dto.MovieDto;
import ru.ravil.models.Actor;
import ru.ravil.models.Movie;

import java.util.List;
import java.util.Optional;

public interface MovieService {
  List<MovieDto> getAllMovies();

  void addMovie(Movie movie);

  Optional<Movie> getMovieById(Long id);

  void addActorFromMovie(Long id, Actor actor);
}
