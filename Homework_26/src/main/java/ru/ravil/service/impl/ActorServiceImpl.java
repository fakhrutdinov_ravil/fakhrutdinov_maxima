package ru.ravil.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ravil.dto.ActorDto;
import ru.ravil.repositories.ActorRepository;
import ru.ravil.service.ActorService;

import java.util.List;

@Service
public class ActorServiceImpl implements ActorService<ActorDto> {
  private ActorRepository actorRepository;

  @Autowired
  public ActorServiceImpl(ActorRepository actorRepository) {
    this.actorRepository = actorRepository;
  }

  @Override
  public List<ActorDto> getAllByMovieId(Long movieId) {
    return ActorDto.from(actorRepository.getAllByMovies_id(movieId));
  }

  @Override
  public List<ActorDto> getAllByMoviesIsNull() {
    return ActorDto.from(actorRepository.getAllByMoviesIsNull());
  }
}
