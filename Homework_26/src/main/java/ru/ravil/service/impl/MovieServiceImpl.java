package ru.ravil.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ravil.dto.MovieDto;
import ru.ravil.models.Actor;
import ru.ravil.models.Movie;
import ru.ravil.repositories.ActorRepository;
import ru.ravil.repositories.MovieRepository;
import ru.ravil.service.MovieService;

import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {

  private ActorRepository actorRepository;
  private MovieRepository movieRepository;

  @Autowired
  public MovieServiceImpl(ActorRepository actorRepository, MovieRepository movieRepository) {
    this.actorRepository = actorRepository;
    this.movieRepository = movieRepository;
  }

  @Override
  public List<MovieDto> getAllMovies() {
    return MovieDto.from(movieRepository.getAllByMovieNameIsNotNull());
  }

  @Override
  public void addMovie(Movie movie) {
    movieRepository.save(movie);
  }

  @Override
  public Optional<Movie> getMovieById(Long id) {
    return Optional.of(movieRepository.getById(id));
  }

  @Override
  public void addActorFromMovie(Long id, Actor actor) {
    Optional<Movie> movieOptional = getMovieById(id);
    Optional<Actor> actorOptional = actorRepository.getById(actor.getId());
    if (movieOptional.isPresent() && actorOptional.isPresent()) {
      Movie movie = movieOptional.get();
      movie.getActors().add(actorOptional.get());
      movieRepository.save(movie);
    } else {
      throw new NullPointerException("Movie with ID: " + id + "doesn't exist" + " or " +
          "Actor with ID: " + actor.getId() + "doesn't exist");
    }
  }
}
