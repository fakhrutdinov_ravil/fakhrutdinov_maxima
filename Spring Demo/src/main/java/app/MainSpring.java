package app;

import models.Car;
import models.CarShop;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import services.BuyCarFromCash;

public class MainSpring {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/context.xml");
        CarShop carShop = (CarShop) applicationContext.getBean("carShop");
        Car car = applicationContext.getBean(Car.class);
        car.setId(3L);
        carShop.getBuyCar().buy(car,5000);

    }

}
