package utils;

import models.Car;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class JdbcParamsUtils {
    public static MapSqlParameterSource getParamsForAdd(Car car) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource
                .addValue("company", car.getCompany())
                .addValue("model", car.getModel())
                .addValue("price", car.getPrice())
                .addValue("sold", car.isSold());
        return parameterSource;
    }

    public static MapSqlParameterSource getParamsForDelete(Car car) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", car.getId());
        return parameterSource;
    }

    public static MapSqlParameterSource getParamsForUpdate(Car car) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource
                .addValue("id", car.getId())
                .addValue("company", car.getCompany())
                .addValue("model", car.getModel())
                .addValue("price", car.getPrice())
                .addValue("sold", car.isSold());
        return parameterSource;
    }

    public static MapSqlParameterSource getParamsForSelectById(long id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource
                .addValue("id", id);
        return parameterSource;
    }
}
