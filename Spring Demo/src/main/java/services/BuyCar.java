package services;

import models.Car;

public interface BuyCar {
    void buy(Car car, int cash);
}
