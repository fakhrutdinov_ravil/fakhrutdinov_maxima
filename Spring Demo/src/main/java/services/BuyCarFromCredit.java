package services;

import models.Car;
import models.CarShop;
import org.springframework.beans.factory.annotation.Autowired;
import repository.CarsRepository;

import java.util.Optional;

public class BuyCarFromCredit implements BuyCar {
    private final CarShop carShop;
    private final CarsRepository carsRepository;

    @Autowired
    public BuyCarFromCredit(CarShop carShop, CarsRepository carsRepository) {
        this.carShop = carShop;
        this.carsRepository = carsRepository;
    }

    @Override
    public void buy(Car car, int cash) {
        Optional<Car> currentCar = carsRepository.findById(car.getId());
        Car buyingCar = null;
        if (currentCar.isPresent()) {
            buyingCar = currentCar.get();
        }
        assert buyingCar != null;
        if (cash>=buyingCar.getPrice()+buyingCar.getPrice()*carShop.getPercent()) {
            buyingCar.setSold(true);
            carsRepository.update(buyingCar);
            System.out.println(buyingCar+"ПРОДАНА!");
        } else {
            System.out.println("Не хватает средств");
        }
    }
}
