package services;

import models.Car;
import models.CarShop;
import org.springframework.beans.factory.annotation.Autowired;
import repository.CarsRepository;

import java.util.Optional;

public class BuyCarFromCash implements BuyCar {
    private final CarsRepository carsRepository;
    private final CarShop carShop;

    @Autowired
    public BuyCarFromCash(CarsRepository carsRepository, CarShop carShop) {
        this.carsRepository = carsRepository;
        this.carShop = carShop;
    }

    @Override
    public void buy(Car car, int cash) {
            Optional<Car> currentCar = carsRepository.findById(car.getId());
            Car buyingCar = null;
            if (currentCar.isPresent()) {
                buyingCar = currentCar.get();
            }
            assert buyingCar != null;
            if (cash>=buyingCar.getPrice()) {
                buyingCar.setSold(true);
                carsRepository.update(buyingCar);
                System.out.println(buyingCar+"ПРОДАНА!");
            } else {
                System.out.println("Не хватает средств");
            }
        }
    }
