package repository;

import models.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import utils.JdbcParamsUtils;

import javax.sql.DataSource;
import java.util.Objects;
import java.util.Optional;

public class CarsRepositoryImpl implements CarsRepository {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public CarsRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public boolean add(Car car) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        //language=SQL
        String SQL_ADD_CAR = "insert into cars(company, model, price) values (:company,:model,:price)";
        int rowsAdd = namedParameterJdbcTemplate.update(SQL_ADD_CAR, JdbcParamsUtils.getParamsForAdd(car), keyHolder, new String[]{"id"});
        if (rowsAdd > 0) {
            car.setId(Objects.requireNonNull(keyHolder.getKey()).longValue());
            System.out.println("Авто " + car + " успешно добавлен в базу данных");
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(Car car) {
        //language=SQL
        String SQL_DELETE_CAR = "delete from cars where id=:id";
        int rowsAdd = namedParameterJdbcTemplate.update(SQL_DELETE_CAR, JdbcParamsUtils.getParamsForDelete(car));
        if (rowsAdd > 0) {
            System.out.println("Авто " + car + " успешно удален из базы данных");
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Car car) {
        //language=SQL
        String SQL_UPDATE_CAR = "update cars set company=:company,model=:model,price=:price, sold=:sold where id=:id";
        int rowsAdd = namedParameterJdbcTemplate.update(SQL_UPDATE_CAR, JdbcParamsUtils.getParamsForUpdate(car));
        if (rowsAdd > 0) {
            System.out.println("Авто " + car + " успешно обновлен в базе данных");
            return true;
        }
        return false;
    }

    @Override
    public Optional<Car> findById(long id ) {
        try {
            //language=SQL
            String SQL_SELECT_BY_ID_CAR = "select * from cars where id=:id";
            return Optional.of(Objects.requireNonNull(namedParameterJdbcTemplate.query(SQL_SELECT_BY_ID_CAR, JdbcParamsUtils.getParamsForSelectById(id), carResultSetExtractor)));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    private final RowMapper<Car> carRowMapper = (resultSet, i) -> Car.builder()
            .id(resultSet.getLong("id"))
            .company(resultSet.getString("company"))
            .model(resultSet.getString("model"))
            .price(resultSet.getInt("price"))
            .sold(resultSet.getBoolean("sold"))
            .build();

    private final ResultSetExtractor<Car> carResultSetExtractor = resultSet -> {
        Car car = null;
        if (resultSet.next()) {
            car = carRowMapper.mapRow(resultSet,0);
        }
        return car;
    };
}
