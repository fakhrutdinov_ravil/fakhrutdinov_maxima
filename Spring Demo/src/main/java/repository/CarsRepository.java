package repository;

import models.Car;

import java.util.Optional;

public interface CarsRepository {
    boolean add(Car car);
    boolean delete(Car car);
    boolean update(Car car);
    Optional<Car> findById(long id);
}
