package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import services.BuyCar;

public class CarShop {
    int percent;
    private BuyCar buyCar;

    @Autowired
    public CarShop(
            @Qualifier(value = "buyCarFromCredit") BuyCar buyCar) {
        this.buyCar = buyCar;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public BuyCar getBuyCar() {
        return buyCar;
    }

    public void setBuyCar(BuyCar buyCar) {
        this.buyCar = buyCar;
    }
}
