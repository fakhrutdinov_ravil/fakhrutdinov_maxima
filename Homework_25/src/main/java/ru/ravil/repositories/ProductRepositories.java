package ru.ravil.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ravil.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepositories extends JpaRepository<Product,Long> {
  @Override
  Optional<Product> findById(Long id);

  List<Product> findByNameLike (String name);
}
