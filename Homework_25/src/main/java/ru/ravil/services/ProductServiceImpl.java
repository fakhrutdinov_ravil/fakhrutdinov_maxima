package ru.ravil.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ravil.dto.ProductDto;
import ru.ravil.repositories.ProductRepositories;

import java.util.List;

import static ru.ravil.dto.ProductDto.from;

@Service
public class ProductServiceImpl implements ProductService {

  private final ProductRepositories repositories;

  @Autowired
  public ProductServiceImpl(ProductRepositories repositories) {
    this.repositories = repositories;
  }


  @Override
  public List<ProductDto> getAllProducts() {
    return from(repositories.findAll());
  }
}
