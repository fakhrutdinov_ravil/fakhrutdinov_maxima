package ru.ravil.services;

import ru.ravil.dto.ProductDto;

import java.util.List;

public interface ProductService {
  List<ProductDto> getAllProducts();
}
