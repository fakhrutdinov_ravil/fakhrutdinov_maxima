package ru.ravil.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.ravil.services.ProductService;

@Controller
public class ProductController {
  private final ProductService productService;

  @Autowired
  public ProductController(ProductService productService) {
    this.productService = productService;
  }

  @RequestMapping("/myproducts")
  public String getProducts(Model model){
    model.addAttribute("products",productService.getAllProducts());
    return "product_page";
  }
}
