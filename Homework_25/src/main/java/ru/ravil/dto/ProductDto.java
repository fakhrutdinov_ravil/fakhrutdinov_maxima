package ru.ravil.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import ru.ravil.models.Product;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto {
  private long id;
  private String name;

  public static ProductDto from(Product product){
    return ProductDto.builder()
        .id(product.getId())
        .name(product.getName())
        .build();
  }

  public static List<ProductDto> from(List<Product> products){
    return products.stream().map(ProductDto::from).collect(Collectors.toList());
  }
}
