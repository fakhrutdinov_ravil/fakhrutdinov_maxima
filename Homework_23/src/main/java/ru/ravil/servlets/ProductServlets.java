package ru.ravil.servlets;

import org.springframework.context.ApplicationContext;
import ru.ravil.models.Product;
import ru.ravil.service.ProductService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/products")
public class ProductServlets extends HttpServlet {
  private static ProductService productService;

  @Override
  public void init(ServletConfig config) throws ServletException {
    ServletContext servletContext = config.getServletContext();
    ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
    productService = (ProductService) applicationContext.getBean("productService");
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try (PrintWriter writer = response.getWriter()) {

      writer.println("<h1>Products</h1>");
      writer.println("<form>");
      writer.println("<label for=nameProduct>Name product:</label>");
      writer.println("<input id=nameProduct type=text name=name placeholder=\"For example: milk\">");
      writer.println("<input type=submit value=Search>");
      writer.println("<p>");
      writer.println("</form>");
      writer.println("<form>");
      writer.println("<label for=nameNewProduct>SAVE product:</label>");
      writer.println("<input id=nameNewProduct type=text name=save placeholder=\"For example: Cheese\">");
      writer.println("<p>");
      writer.println("<label for=countNewProduct>COUNT product:</label>");
      writer.println("<input id=countNewProduct type=text name=count placeholder=\"For example: 1\">");
      writer.println("<input type=submit value=SAVE>");
      writer.println("<form>");
      writer.println("<p>");

      writer.println("<table>");
      for (Product product : productService.getProducts()) {
        writer.println("<tr>");
        writer.println("    <td>" + product.getName() + "  " + product.getCount() + "</td>");
        writer.println("</tr>");
      }

      writer.println("</table>");

      if (request.getParameter("name") != null && request.getParameter("name").equalsIgnoreCase("milk")) {
        response.sendRedirect("/productsearch");
      }

      if (request.getParameter("save") != null && !request.getParameter("save").isEmpty()) {
        String nameProduct = request.getParameter("save");
        int count = Integer.parseInt(request.getParameter("count"));
        productService.saveProduct(nameProduct, count);
        response.sendRedirect("/products");
      }
    }
  }
}
