package ru.ravil.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.util.Objects;

@PropertySource(value = "classpath:application.properties")
@ComponentScan(value = "ru.ravil")
@Configuration(value = "appConfig")
public class ApplicationConfig {
  private final Environment environment;

  @Autowired
  public ApplicationConfig(Environment environment) {
    this.environment = environment;
  }

  @Bean
  public DataSource dataSource(HikariConfig hikariConfig) {
    return new HikariDataSource(hikariConfig);
  }

  @Bean
  public HikariConfig hikariConfig(){
    HikariConfig hikariConfig = new HikariConfig();
    hikariConfig.setJdbcUrl(environment.getProperty("db.url"));
    hikariConfig.setDriverClassName(environment.getProperty("db.driverClassName"));
    hikariConfig.setUsername(environment.getProperty("db.user"));
    hikariConfig.setPassword(environment.getProperty("db.password"));
    hikariConfig.setMaximumPoolSize(Integer.parseInt(Objects.requireNonNull(environment.getProperty("db.hikari.max-pool-size"))));
    return hikariConfig;
  }
}
