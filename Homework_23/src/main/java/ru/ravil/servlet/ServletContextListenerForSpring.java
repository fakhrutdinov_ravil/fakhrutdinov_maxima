package ru.ravil.servlet;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ravil.config.ApplicationConfig;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ServletContextListenerForSpring implements ServletContextListener {

  private static ApplicationContext applicationContext;

  @Override
  public void contextInitialized(ServletContextEvent sce) {
    applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
    ServletContext context = sce.getServletContext();
    context.setAttribute("springContext",applicationContext);
  }

  @Override
  public void contextDestroyed(ServletContextEvent servletContextEvent) {
    HikariDataSource hikariDataSource = applicationContext.getBean(HikariDataSource.class);
    hikariDataSource.close();
  }
}
