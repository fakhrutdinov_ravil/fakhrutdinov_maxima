package ru.ravil.service;

import ru.ravil.models.Product;

import java.util.List;

public interface ProductService {
  void saveProduct(String productName,int countProduct);

  List<Product> getProducts();

  List<String> getProductsName();

  List<Product> getMilkProduct();
}
