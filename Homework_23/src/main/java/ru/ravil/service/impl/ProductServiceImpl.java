package ru.ravil.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ravil.models.Product;
import ru.ravil.repositories.ProductRepositories;
import ru.ravil.service.ProductService;

import java.util.List;
import java.util.stream.Collectors;

@Service(value = "productService")
public class ProductServiceImpl implements ProductService {
  private final ProductRepositories<Product> productRepositories;

  @Autowired
  public ProductServiceImpl(ProductRepositories<Product> productRepositories) {
    this.productRepositories = productRepositories;
  }

  @Override
  public void saveProduct(String productName, int countProduct) {
    Product product = Product.builder()
        .name(productName)
        .count(countProduct)
        .build();
    productRepositories.save(product);
  }

  @Override
  public List<Product> getProducts() {
    return productRepositories.getAllProducts();
  }

  @Override
  public List<String> getProductsName() {
    return productRepositories.getAllProducts()
        .stream()
        .map(Product::getName)
        .collect(Collectors.toList());
  }

  @Override
  public List<Product> getMilkProduct() {
    return productRepositories.getAllProducts()
        .stream()
        .filter(product -> product.getName().toLowerCase().contains("milk"))
        .collect(Collectors.toList());
  }
}
