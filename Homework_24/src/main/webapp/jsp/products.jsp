<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Продукты</title>
</head>
<script>
    function searchProducts(name) {
        if (name.length >= 3) {
            let request = new XMLHttpRequest();
            request.open('GET', '/products?name=' + name, false);
            request.send();
            if (request.status !== 200) {
                alert("ERROR!")
            } else {
                let html = '<tr>' +
                    '<th>Id</th>' +
                    '<th>Name</th>' +
                    '<th>Count</th>' +
                    '</tr>';
                let response = JSON.parse(request.response);
                for (let i = 0; i < response['products'].length; i++) {
                    html += '<tr>';
                    html += '<td style="text-align: center">' + response['products'][i]['id'] + '</td>';
                    html += '<td style="text-align: center">' + response['products'][i]['name'] + '</>';
                    html += '<td style="text-align: center">' + response['products'][i]['count'] + '</>';
                    html += '</tr>';
                }
                document.getElementById('products_table').innerHTML = html;
            }
        } else {
            fillTableHtml();
        }
    }

    //Так как функция выше меняет html таблицы, когда мы стираем из поиска ввода html таблицы остается старым
    //Данная функция возвращает html таблице.
    //Если я уберу html строки из таблицы внизу то:
    //То не понятно :) почему у меня отсутсвует таблица когда я стираю текст из строки поиска.
    function fillTableHtml() {
        let html = '<tr>' +
            '<th>Id</th>' +
            '<th>Name</th>' +
            '<th>Count</th>' +
            '</tr>' +
            '<c:forEach items="${products}" var="product">' +
            '<tr>' +
            '<td>${product.id}</td>' +
            '<td>${product.name}</td>' +
            '<td>${product.count}</td>' +
            '</tr>' +
            '</c:forEach>'
        document.getElementById('products_table').innerHTML = html;
    }

</script>
<body>
<h3>Find</h3>
<form>
    <label for=nameProduct>Name product:</label>
    <input id=nameProduct type=text name=name required size="10"
           onkeyup="searchProducts(document.getElementById('nameProduct').value)">
    <br>
    <br>
    <h3>Save</h3>
</form>
<form method="post">
    <input id=nameNewProduct type=text name=save size="10">
    <label for=nameNewProduct>: Product Name</label>
    <p>
        <input id=countNewProduct type=number required name=count min="1" value="1" step="1" style="width: 0.9cm">
        <label for=countNewProduct>: Product count</label>
    <p></p>
    <input type=submit value=SAVE>
</form>
<p>
    <br>
    <br>
<h3>Count of Products - ${products.size()} </h3>

<table id="products_table">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Count</th>
    </tr>
    <c:forEach items="${products}" var="product">
        <tr>
            <td>${product.id}</td>
            <td>${product.name}</td>
            <td>${product.count}</td>
        </tr>
    </c:forEach>
</table>
</form>
</body>
</html>
