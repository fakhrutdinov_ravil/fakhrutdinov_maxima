<%@ page import="java.util.List" %>
<%@ page import="ru.ravil.models.Product" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Count of Products - <%=((List<Product>) request.getAttribute("products")).size()%>
</h1>
<table>
    <tr>
        <th>Name</th>
        <th>Count</th>
    </tr>
    <%
        List<Product> products = (List<Product>) request.getAttribute("products");
        for (Product product : products) {
    %>
    <tr>
        <td><%=product.getName()%>
        </td>
        <td><%=product.getCount()%>
        </td>
    </tr>
    <%
        }
    %>
</table>
</body>
</html>
