package ru.ravil.repositories;

import ru.ravil.models.Product;

import java.util.List;

public interface ProductRepositories<P extends Product> {
  void save(P product);
  List<P> getAllProducts();
  void update(P product);
  List<P> findByName(String name);
}
