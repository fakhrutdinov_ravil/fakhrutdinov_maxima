package ru.ravil.repositories.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.ravil.models.Product;
import ru.ravil.repositories.ProductRepositories;
import ru.ravil.utils.ParameterSource;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unchecked")
@Repository
public class ProductRepositoriesNameParameterJdbc<P extends Product> implements ProductRepositories<P> {

  private static final String SQL_INSERT = "insert into product(product_name,count) values (:productName,:count)";

  private static final String SQL_SELECT = "select * from product order by id";

  private static final String SQL_UPDATE = "update product set count= :count where product_name=:productName";

  private static final String SQL_CHECK_PRODUCT = "select * from product where product_name=:name";

  private static final String SQL_SELECT_BY_NAME = "select * from product where product_name like :name";

  private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  private final RowMapper<P> productRowMapper = (resultSet, i) -> (P) Product.builder()
      .id(resultSet.getLong("id"))
      .name(resultSet.getString("product_name"))
      .count(resultSet.getInt("count"))
      .build();


  @Autowired
  public ProductRepositoriesNameParameterJdbc(DataSource dataSource) {
    this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
  }

  @Override
  public void save(P product) {
    KeyHolder keyHolder = new GeneratedKeyHolder();
    //Проверка, а есть ли такой продукт вообще или нет.
    List<P> list = namedParameterJdbcTemplate.query(SQL_CHECK_PRODUCT,
        Collections.singletonMap("name", product.getName()),
        productRowMapper);
    //Если не было то создаем новый
    //Иначе обновляем значение count
    if (list.isEmpty()) {
      namedParameterJdbcTemplate.update(SQL_INSERT,
          new MapSqlParameterSource()
              .addValue("productName", product.getName())
              .addValue("count", product.getCount()), keyHolder, new String[]{"id"});
      product.setId((Long) keyHolder.getKey());
    } else {
      update(product);
    }
  }

  @Override
  public List<P> getAllProducts() {
    return namedParameterJdbcTemplate.query(SQL_SELECT, productRowMapper);
  }

  @Override
  public void update(P product) {
    namedParameterJdbcTemplate.update(SQL_UPDATE, ParameterSource.sqlParameterSourceUpdate(product));
  }

  @Override
  public List<P> findByName(String name) {
    String finalName = "%"+name.trim()+"%";
    return namedParameterJdbcTemplate.query(SQL_SELECT_BY_NAME, Collections.singletonMap("name", finalName), productRowMapper);
  }
}
