package ru.ravil.utils;

import ru.ravil.models.Product;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ParameterSource {
  private ParameterSource() {
    throw new IllegalStateException("Utility class");
  }

  public static SqlParameterSource sqlParameterSourceUpdate(Product product){
    return new MapSqlParameterSource()
        .addValue("productName",product.getName())
        .addValue("count",product.getCount())
        .addValue("id",product.getCount());
  }

}
