package ru.ravil.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.ravil.models.Product;
import ru.ravil.models.ProductsResponse;
import ru.ravil.service.ProductService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/products")
public class ProductServlets extends HttpServlet {
  private static ProductService productService;
  private static ObjectMapper objectMapper;

  @Override
  public void init(ServletConfig config) throws ServletException {
    ServletContext servletContext = config.getServletContext();
    ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
    productService = (ProductService) applicationContext.getBean("productService");
    objectMapper = applicationContext.getBean(ObjectMapper.class);
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    if (request.getParameter("name") == null) {
      List<Product> products = productService.getProducts();
      request.setAttribute("products", products);
      request.getRequestDispatcher("/jsp/products.jsp").forward(request, response);
    } else {
      List<Product> products = productService.findProductByName(request.getParameter("name"));
      String jsonResponse = objectMapper.writeValueAsString(new ProductsResponse(products));
      response.setStatus(200);
      response.setContentType("application/json");
      response.getWriter().println(jsonResponse);
    }

  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String name = request.getParameter("save");
    Integer count = Integer.valueOf(request.getParameter("count"));
    productService.saveProduct(name, count);
    response.sendRedirect("/products");
  }
}


