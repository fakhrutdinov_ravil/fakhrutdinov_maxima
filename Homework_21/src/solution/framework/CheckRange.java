package solution.framework;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//Позволяет указать в какой момент жизни программного кода будет доступна аннотация.
@Retention(value = RetentionPolicy.RUNTIME)
//Какой элемент программы будет использоваться аннотацией.
@Target(value = ElementType.FIELD)
public @interface CheckRange {
    int min();
    int max();
}
