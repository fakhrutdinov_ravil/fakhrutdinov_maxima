package solution.framework;

public class Sertificate implements Document {
    @CheckRange(min = 0, max = 10)
    private int activeYears;

    public Sertificate(int activeYears) {
        this.activeYears = activeYears;
    }
}

